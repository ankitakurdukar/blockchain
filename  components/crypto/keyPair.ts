import { ec as EC } from "elliptic";
const ec = new EC("secp256k1");

export class KeyPair {
  keyPair: EC.KeyPair;

  static getKeyPair = () => ec.genKeyPair();
  static getKeyFromPublic = (pubKey: string) => ec.keyFromPublic(pubKey, "hex");
  static getKeyFromPrivate = (ppk: string) => ec.keyFromPrivate(ppk);
  static verifySignature = (
    pubKey: string,
    txHash: string,
    signature: string
  ) => {
    const publicKey = ec.keyFromPublic(pubKey, "hex");
    return publicKey.verify(txHash, signature);
  };

  constructor(kP: EC.KeyPair) {
    this.keyPair = kP;
  }

  getPublic = () => this.keyPair.getPublic("hex");

  sign = (message: string) => {
    return this.keyPair.sign(message, "base-64");
  };
}
