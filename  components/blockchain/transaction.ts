import { SHA256 } from "crypto-js";
import { KeyPair } from "../crypto/keyPair";
export class Transaction {
  fromAddress: null | string;
  toAddress: string;
  amount: number;
  signature?: any;

  constructor(from: null | string, to: string, amount: number) {
    this.fromAddress = from;
    this.toAddress = to;
    this.amount = amount;
  }

  calculateHash() {
    return SHA256(this.fromAddress + this.toAddress + this.amount).toString();
  }

  signTransaction(signingKey: KeyPair) {
    if (signingKey.getPublic() !== this.fromAddress)
      throw new Error("You cannot sign transactions on other wallets!");

    const txHash = this.calculateHash();
    const signature = signingKey.sign(txHash);
    this.signature = signature.toDER("hex");
  }

  isTxValid() {
    if (this.fromAddress === null) return true;
    if (!this.signature || !this.signature.length)
      throw new Error("No signature present!");

    return KeyPair.verifySignature(
      this.fromAddress,
      this.calculateHash(),
      this.signature
    );
  }
}
