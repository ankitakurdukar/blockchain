import SHA256 from "crypto-js/sha256";
import { Transaction } from "./transaction";

export class Block {
  timestamp: string;
  transactions: Transaction[];
  previousHash: string;
  nonce: number;
  hash: string;

  constructor(
    timestamp: string,
    transactions: Transaction[],
    previousHash?: string
  ) {
    this.timestamp = timestamp;
    this.transactions = transactions;
    this.previousHash = previousHash || "0";
    this.nonce = 0;
    this.hash = this.calculateHash();
  }

  mineBlock(difficulty: number) {
    while (
      this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")
    ) {
      this.nonce++;
      this.hash = this.calculateHash();
    }
    console.log("Block mined: " + this.hash);
  }

  hasValidTransactions() {
    for (const tx of this.transactions) {
      if (!tx.isTxValid) return false;
    }
    return true;
  }

  calculateHash() {
    return SHA256(
      this.timestamp +
        JSON.stringify(this.transactions) +
        this.previousHash +
        this.nonce
    ).toString();
  }
}
