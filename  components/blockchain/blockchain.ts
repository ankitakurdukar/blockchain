import SHA256 from "crypto-js/sha256";
import { Block } from "./block";
import { Transaction } from "./transaction";

export class Blockchain {
  chain: Block[];
  difficulty: number;
  pendingTransactions: Transaction[];
  miningReward: number;

  constructor() {
    this.chain = [this.createGenesisBlock()];
    this.difficulty = 2;
    this.pendingTransactions = [];
    this.miningReward = 100;
  }

  addTransaction(transaction: Transaction) {
    if (!transaction.fromAddress || !transaction.toAddress)
      throw new Error("Transaction must have 'from' and 'to' addresses");
    if (!transaction.isTxValid())
      throw new Error("Cannot add invalid transaction to chain!");

    this.pendingTransactions.push(transaction);
  }

  createGenesisBlock(): Block {
    return new Block("01/01/2022", [], "");
  }

  getLatestBlock() {
    return this.chain[this.chain.length - 1];
  }

  getBalanceOfAddress(address: string) {
    let balance = 0;
    for (const block of this.chain) {
      for (const t of block.transactions) {
        if (t.fromAddress === address) balance -= t.amount;
        if (t.toAddress === address) balance += t.amount;
      }
    }
    return balance;
  }

  minePendingTransactions(miningRewardAddress: string) {
    const rewardTx = new Transaction(
      null,
      miningRewardAddress,
      this.miningReward
    );
    this.pendingTransactions.push(rewardTx);
    const newBlock = new Block(
      Date.now().toString(),
      this.pendingTransactions,
      this.getLatestBlock().hash
    );
    newBlock.previousHash = this.getLatestBlock().hash;
    if (!newBlock.hasValidTransactions())
      throw new Error("Block has invalid transactions. Cannot mine!");

    this.pendingTransactions = [
      new Transaction(null, miningRewardAddress, this.miningReward),
    ];

    newBlock.mineBlock(this.difficulty);
    console.log("Block successfully mined!");

    this.chain.push(newBlock);
    this.pendingTransactions = [];
  }

  isChainValid() {
    for (let i = 1; i < this.chain.length; i++) {
      const [previousBlock, currentBlock] = [this.chain[i - 1], this.chain[i]];

      if (!currentBlock.hasValidTransactions) return false;

      if (
        currentBlock.hash !== currentBlock.calculateHash() ||
        currentBlock.previousHash !== previousBlock.hash
      )
        return false;
    }
    return true;
  }
}
